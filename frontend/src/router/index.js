import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/search',
    name: 'search',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/SearchView.vue')
  },
  {
    path: '/fantacitorio',
    name: 'fanta',
    component: () => import(/* webpackChunkName: "fantacitorio" */ '../views/FantacitorioView.vue')
  },
  {
    path: '/ghigliottina',
    name: 'ghigliottina',
    component: () => import(/* webpackChunkName: "ghigliottina" */ '../views/GhigliottinaView.vue')
  },
  {
    path: '/chess',
    name: 'chess',
    component: () => import('../views/ChessView.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
