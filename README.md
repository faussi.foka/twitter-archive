# Twitter-Archive

Progetto di gruppo svolto per il corso di Ingegneria del Software nell'università di Bologna.
Il progetto è stato svolto con Vue.js.

È possibile avere maggiori informazioni leggendo il file "relazione_progetto_Team5_SWE.pdf".

È possibile scoprire la consegna del progetto leggendo il file "testo_progetto.pdf".
