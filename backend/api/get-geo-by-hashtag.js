
import { client } from "../utils/twitter-client.js";
import { tweetFields } from "../utils/fields.js";

export default async function getGeoByHashtag(hashtag, startDate, endDate){
    
    const geoHashtag = await client.tweets.tweetsFullarchiveSearch({
        "query": hashtag + " has:geo",
        "start_time": startDate,
        "end_time": endDate,
        "tweet.fields": tweetFields
      });

      return geoHashtag;
}