
import { client  } from "../utils/twitter-client.js"

const max_result = 100;

export default async function getRetweetGame(id) {
    let tweets = await client.tweets.findTweetsThatQuoteATweet( id, {
        "tweet.fields": ["text"],
        "max_results": max_result
    })
    return tweets
}