
import { client } from "../utils/twitter-client.js";
import { tweetFields, userFields, numberOfTweets500 } from "../utils/fields.js";

export default async function getSentimentByUser(user, startDate, endDate, token) {

    let sentiment;

    if(token === undefined || token === 'empty'){
        sentiment = await client.tweets.tweetsFullarchiveSearch({
            'query': user,
            'max_results': numberOfTweets500,
            'start_time': startDate,
            'end_time': endDate,
            'expansions': 'author_id',
            'tweet.fields': tweetFields,
            'user.fields': userFields
        });
    } else {
        sentiment = await client.tweets.tweetsFullarchiveSearch({
            'query': user,
            'max_results': numberOfTweets500,
            'start_time': startDate,
            'end_time': endDate,
            'expansions': 'author_id',
            'pagination_token': token,
            'tweet.fields': tweetFields,
            'user.fields': userFields
        });
    }
    
    return sentiment;
}