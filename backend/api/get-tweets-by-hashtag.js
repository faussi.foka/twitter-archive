
import { client } from "../utils/twitter-client.js";
import { tweetFields, userFields, numberOfTweets30 } from "../utils/fields.js";

export default async function getTweetsByHashtag(hashtag, startDate, endDate, token) {

    let tweets;

    if(token === undefined || token === 'empty'){
        tweets = await client.tweets.tweetsFullarchiveSearch({
            'query': hashtag,
            'max_results': numberOfTweets30,
            'start_time': startDate,
            'end_time': endDate,
            'expansions': 'author_id',
            'tweet.fields': tweetFields,
            'user.fields': userFields
        });
    } else {
        tweets = await client.tweets.tweetsFullarchiveSearch({
            'query': hashtag,
            'max_results': numberOfTweets30,
            'start_time': startDate,
            'end_time': endDate,
            'expansions': 'author_id',
            'pagination_token': token,
            'tweet.fields': tweetFields,
            'user.fields': userFields
        });
    }
    
    return tweets;
}