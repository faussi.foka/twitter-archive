import { client } from "../utils/twitter-client.js";
import { tweetFields, userFields, numberOfTweets500 } from "../utils/fields.js";

let hashtag = 'leredita'

export default async function getAttempts(start, end, token) {

    let tweets;

    if(token === undefined || token === 'empty'){
        tweets = await client.tweets.tweetsFullarchiveSearch({
            'query': hashtag,
            'max_results': numberOfTweets500,
            'start_time': start,
            'end_time': end,
            'expansions': 'author_id',
            'tweet.fields': tweetFields,
            'user.fields': userFields
        });
    } else {
        tweets = await client.tweets.tweetsFullarchiveSearch({
            'query': hashtag,
            'max_results': numberOfTweets500,
            'start_time': start,
            'end_time': end,
            'expansions': 'author_id',
            'pagination_token': token,
            'tweet.fields': tweetFields,
            'user.fields': userFields
        });
    }
    return tweets;
}

