import { client } from "../utils/twitter-client.js";
import { tweetFields, userFields, numberOfTweets30 } from "../utils/fields.js";

export default async function getWord(startDate, endDate) {

    let tweets;

        tweets = await client.tweets.tweetsFullarchiveSearch({
            'query': 'from:quizzettone #parola #ghigliottina -is:reply -is:retweet -is:quote',
            'start_time': startDate,
            'end_time': endDate,
            'expansions': 'author_id',
            'max_results': numberOfTweets30,
            'tweet.fields': tweetFields,
            'user.fields': userFields
        })
    
    return tweets;
}