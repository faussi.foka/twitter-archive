
import { client } from "../utils/twitter-client.js";
import { tweetFields } from "../utils/fields.js";

const max_result = 500;

export default async function getTermCloud(keyword, startDate, endDate, token) {

    let cloud;

    if(token === undefined || token === 'empty'){
        cloud = await client.tweets.tweetsFullarchiveSearch({
            'query': keyword,
            'max_results': max_result,
            'start_time': startDate,
            'end_time': endDate,
            'tweet.fields': [
                tweetFields,
                'lang'
            ]
            
        });
    } else {
        cloud = await client.tweets.tweetsFullarchiveSearch({
            'query': keyword,
            'max_results': max_result,
            'start_time': startDate,
            'end_time': endDate,
            'pagination_token': token,
            'tweet.fields': [
                tweetFields,
                'lang'
            ]
        });
    }
    return cloud;
}
