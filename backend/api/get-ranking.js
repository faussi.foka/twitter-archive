
import { client } from '../utils/twitter-client.js';
import { tweetFields, numberOfTweets500 } from '../utils/fields.js';


const keyword = "from:Fanta_citorio";

export default async function getRanking(startDate, endDate) {
    const ranking = await client.tweets.tweetsFullarchiveSearch({
        "query": keyword,
        "start_time": startDate,
        "end_time": endDate,
        "max_results": numberOfTweets500,
        "tweet.fields": tweetFields
    });
    return ranking
}








/* app.get('/getranking/:start/:end', async (req, res) \=> {
const keyword \= "from:Fanta\_citorio"
const startdate \= req.params.start + "T00:00:00.000Z"
const enddate \= req.params.end + "T00:00:00.000Z"
let result \= await client.tweets.tweetsFullarchiveSearch({
"query": keyword,
"start_time": startdate,
"end_time": enddate,
"max_results": 500,
"tweet.fields": [
"text"
]
});
let tweets_text \= []
for (let i \= 0; i < result.data.length; i++) {
let tmp \= result.data[i].text
tweets_text.push(tmp)
}
while(result.meta.hasOwnProperty('next_token')) {
let next_token \= result.meta.next_token
result \= await client.tweets.tweetsFullarchiveSearch({
"query": keyword,
"start_time": startdate,
"end_time": enddate,
"next_token": next_token,
"max_results": 500,
"tweet.fields": [
"text"
],
});
for (let i \= 0; i < result.data.length; i++) {
let tmp \= result.data[i].text
tweets_text.push(tmp)
}
}
let final_result \= require('./fantacitorio').ranking(tweets_text)
for (let i \= 0; i < final_result.length; i++) {
final_result[i].position \= i + 1
}
res.send(final_result)
}) */