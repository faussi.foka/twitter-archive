
import { client } from "../utils/twitter-client.js";
import { tweetFields } from "../utils/fields.js";

export default async function getGeoByUser(user, startDate, endDate){
    
    const geoUser = await client.tweets.tweetsFullarchiveSearch({
        "query": user + " has:geo",
  
        "start_time": startDate,
  
        "end_time": endDate,
  
        "tweet.fields": tweetFields
      });

      return geoUser;
}