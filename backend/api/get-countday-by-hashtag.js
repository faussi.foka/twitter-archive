
import { client } from "../utils/twitter-client.js";
import { searchCountFields } from "../utils/fields.js"; 

export default async function getCountDayByHashtag(hashtag, startDate, endDate, token){
    let countDay;

    if(token === undefined || token === 'empty'){
        countDay = await client.tweets.tweetCountsFullArchiveSearch({
            "query": hashtag,
            "start_time": startDate,
            "end_time": endDate,
            "granularity": "day",
            "search_count.fields": searchCountFields
          });
    } else {
        countDay = await client.tweets.tweetCountsFullArchiveSearch({
            "query": hashtag,
            "start_time": startDate,
            "end_time": endDate,
            "granularity": "day",
            "next_token": token,
            "search_count.fields": searchCountFields
          });
    }

    return countDay;
}