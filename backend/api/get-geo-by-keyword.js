
import { client } from "../utils/twitter-client.js";
import { tweetFields } from "../utils/fields.js";

export default async function getGeoByKeyword(keyword, startDate, endDate){
    
    const geoKeyword = await client.tweets.tweetsFullarchiveSearch({
        "query": keyword + " has:geo",
  
        "start_time": startDate,
  
        "end_time": endDate,
  
        "tweet.fields": tweetFields
      });

      return geoKeyword;
}