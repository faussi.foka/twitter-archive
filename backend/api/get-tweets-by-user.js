
import { client } from "../utils/twitter-client.js";
import { tweetFields, userFields, numberOfTweets30 } from "../utils/fields.js";

export default async function getTweetsByUser(user, startDate, endDate, token) {

    let tweets;

    if(token === undefined || token === 'empty'){
        tweets = await client.tweets.tweetsFullarchiveSearch({
            'query': user + ' -is:reply -is:retweet -is:quote',
            'start_time': startDate,
            'end_time': endDate,
            'expansions': 'author_id',
            'max_results': numberOfTweets30,
            'tweet.fields': tweetFields,
            'user.fields': userFields,
        });
    } else {
        tweets = await client.tweets.tweetsFullarchiveSearch({
            'query': user + ' -is:reply -is:retweet -is:quote',
            'start_time': startDate,
            'end_time': endDate,
            'expansions': 'author_id',
            'max_results': numberOfTweets30,
            'pagination_token': token,
            'tweet.fields': tweetFields,
            'user.fields': userFields,
        });
    }
    
    return tweets;
}