
import { client } from "../utils/twitter-client.js";
import { tweetFields, userFields, numberOfTweets500 } from "../utils/fields.js";

const user = "from:Fanta_citorio"
const startDate = "2022-10-28T00:00:00.000Z"
const endDate = "2022-10-28T23:59:00.00Z"

export default async function getTeamsFantacitorio(next_token) {
    let tweets = await client.tweets.tweetsFullarchiveSearch({
        'query': user,
        'start_time': startDate,
        'end_time': endDate,
        'max_results': numberOfTweets500,
        'tweet.fields': tweetFields,
        'user.fields': userFields,
        "media.fields": [
            "url"
        ],
        'next_token': next_token,
        'expansions': 'attachments.media_keys'
    })
    return tweets
}