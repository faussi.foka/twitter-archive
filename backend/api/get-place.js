
import { clientv1 } from "../utils/twitter-client.js";

export default async function getPlace(place) {
    const coord = await clientv1.v1.geoPlace(place);

   return coord;
}