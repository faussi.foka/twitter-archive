import getTermCloud from '../api/get-term-cloud';
import { client } from '../utils/twitter-client';

jest.mock("../utils/twitter-client");

const tweet_response_emptytoken = {
    data: {
        id: 1,
        edit_history_tweet_ids: [],
        author_id: 1,
        text: 'tweet-text',
        lang: 'en'
    }
};

const tweet_response_fulltoken = {
    data: {
        id: 1,
        edit_history_tweet_ids: [],
        author_id: 1,
        text: 'tweet-text',
        lang: 'en',
        next_token: 'token'
    }
};


describe('getTermCloud tests', ()=>{
    it('no sense keyword', async ()=>{

        const noSenseKeyword = 'ioergaherjciksh';
        // casual value
        const startdate = '2022-12-04T00:00:00.000Z';
        const enddate = '2022-12-07T00:00:00.000Z';
        const token = 'empty';

        await client.tweets.tweetsFullarchiveSearch.mockReturnValue({meta: {result_count:0}})

        const expected = await getTermCloud(noSenseKeyword, startdate, enddate, token);

        expect(expected).toStrictEqual({meta: {result_count:0}})
    }) 

    it('case empty token: valid keyword, test tweets object properties', async () => {

        const validKeyword = 'juventus';
        // casual date
        const startdate = '2022-12-04T00:00:00.000Z';
        const enddate = '2022-12-07T00:00:00.000Z';
        const token = 'empty'; 

        await client.tweets.tweetsFullarchiveSearch.mockReturnValue(tweet_response_emptytoken)

        const expected = await getTermCloud(validKeyword, startdate, enddate, token);

        expect(expected).toHaveProperty('data.id');
        expect(expected).toHaveProperty('data.text');
        expect(expected).toHaveProperty('data.author_id');
        expect(expected).toHaveProperty('data.edit_history_tweet_ids'); 
        expect(expected).toHaveProperty('data.lang');
        expect(expected).not.toHaveProperty('data.next_token');
    })

    it('case full token: valid keyword, test tweets object properties', async () => {

        const validKeyword = 'juventus';
        // casual date
        const startdate = '2022-12-04T00:00:00.000Z';
        const enddate = '2022-12-07T00:00:00.000Z';
        const token = 'token'; 

        await client.tweets.tweetsFullarchiveSearch.mockReturnValue(tweet_response_fulltoken)

        const expected = await getTermCloud(validKeyword, startdate, enddate, token);

        expect(expected).toHaveProperty('data.id');
        expect(expected).toHaveProperty('data.text');
        expect(expected).toHaveProperty('data.author_id');
        expect(expected).toHaveProperty('data.edit_history_tweet_ids'); 
        expect(expected).toHaveProperty('data.lang');
        expect(expected).toHaveProperty('data.next_token');
    })
})