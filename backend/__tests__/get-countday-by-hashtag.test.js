import getCountDayByHashtag from '../api/get-countday-by-hashtag'
import { client } from "../utils/twitter-client";

jest.mock("../utils/twitter-client");

const tweet_response_emptytoken = {
    data: {
        end: '2022-12-05T00:00:00.000Z',
        start: '2022-12-04T00:00:00.000Z',
        tweet_count: 0
    }
};

const tweet_response_fulltoken = {
    data: {
        end: '2022-12-05T00:00:00.000Z',
        start: '2022-12-04T00:00:00.000Z',
        tweet_count: 0,
        next_token: 'token'
    }
};


describe('getCountDayByHashtag tests', ()=>{
    it('no sense hashtag', async ()=>{

        const noSenseHashtag = '#wtyagijkdvershc';
        // casual value
        const startdate = '2022-12-04T00:00:00.000Z';
        const enddate = '2022-12-07T00:00:00.000Z';
        const token = 'empty';

        await client.tweets.tweetCountsFullArchiveSearch.mockReturnValue({meta: {total_tweet_count:0}})

        const expected = await getCountDayByHashtag(noSenseHashtag, startdate, enddate, token);

        expect(expected).toStrictEqual({meta: {total_tweet_count:0}})
    })

    it('case empty token: valid hashtag, test tweets object properties', async () => {

        const validHashtag = '#italia';
        // casual date
        const startdate = '2022-12-04T00:00:00.000Z';
        const enddate = '2022-12-07T00:00:00.000Z';
        const token = 'empty'; 

        await client.tweets.tweetCountsFullArchiveSearch.mockReturnValue(tweet_response_emptytoken)

        const expected = await getCountDayByHashtag(validHashtag, startdate, enddate, token);

        expect(expected).toHaveProperty('data.end');
        expect(expected).toHaveProperty('data.start');
        expect(expected).toHaveProperty('data.tweet_count');
        expect(expected).not.toHaveProperty('data.next_token');
    })

    it('case full token: valid hashtag, test tweets object properties', async () => {

        const validHashtag = '#italia';
        // casual date
        const startdate = '2022-12-04T00:00:00.000Z';
        const enddate = '2022-12-07T00:00:00.000Z';
        const token = 'token'; 

        await client.tweets.tweetCountsFullArchiveSearch.mockReturnValue(tweet_response_fulltoken)

        const expected = await getCountDayByHashtag(validHashtag, startdate, enddate, token);

        expect(expected).toHaveProperty('data.end');
        expect(expected).toHaveProperty('data.start');
        expect(expected).toHaveProperty('data.tweet_count'); 
        expect(expected).toHaveProperty('data.next_token');
    })
})
