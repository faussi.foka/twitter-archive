import getWord from "../api/get-word";
import { client } from '../utils/twitter-client'

jest.mock('../utils/twitter-client')

const tweet_response = {
    data: {
        id: 1,
        edit_history_tweet_ids: [],
        author_id: 1,
        text: 'tweet-text',
    }
};

describe('getWord tests', ()=>{
    
    it('valid dates, test tweets object properties', async () => {

        const startdate = '2022-12-04T00:00:00.000Z';
        const enddate = '2022-12-07T00:00:00.000Z';

        await client.tweets.tweetsFullarchiveSearch.mockReturnValue(tweet_response);

        const expected = await getWord(startdate, enddate);

        expect(expected).toHaveProperty('data.id');
        expect(expected).toHaveProperty('data.text');
        expect(expected).toHaveProperty('data.author_id');
        expect(expected).toHaveProperty('data.edit_history_tweet_ids'); 
        expect(expected).not.toHaveProperty('data.pagination_token');
    })
})