import getGeoByUser from '../api/get-geo-by-user'
import { client } from '../utils/twitter-client'

jest.mock("../utils/twitter-client");

const tweet_response = {
    data: {
        id: 1,
        edit_history_tweet_ids: [],
        author_id: 1,
        text: 'tweet-text',
        geo: {
            place_id: 1
        }
    }
};

describe('getGeoByUser tests', ()=>{
    it('no sense user', async ()=>{

        const noSenseUser = 'from:adkljjafjw has:geo';
        // casual value
        const startdate = '2022-12-04T00:00:00.000Z';
        const enddate = '2022-12-07T00:00:00.000Z';
 

        await client.tweets.tweetsFullarchiveSearch.mockReturnValue({meta: {result_count:0}})

        const expected = await getGeoByUser(noSenseUser, startdate, enddate);

        expect(expected).toStrictEqual({meta: {result_count:0}})
    })

    it('valid user, test tweets object properties', async () => {

        const validUser = 'from:elonmusk has:geo';
        // casual value
        const startdate = '2022-12-04T00:00:00.000Z';
        const enddate = '2022-12-07T00:00:00.000Z';

        await client.tweets.tweetsFullarchiveSearch.mockReturnValue(tweet_response)

        const expected = await getGeoByUser(validUser, startdate, enddate);

        expect(expected).toHaveProperty('data.id');
        expect(expected).toHaveProperty('data.text');
        expect(expected).toHaveProperty('data.author_id');
        expect(expected).toHaveProperty('data.edit_history_tweet_ids');
        expect(expected).toHaveProperty('data.geo.place_id');
        expect(expected).not.toHaveProperty('data.pagination_token');
    })
})