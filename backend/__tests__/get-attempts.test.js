import getAttempts from '../api/get-attempts';
import { client } from "../utils/twitter-client.js";

jest.mock("../utils/twitter-client");

const tweet_response_emptytoken = {
    data: {
        id: 1,
        edit_history_tweet_ids: [],
        author_id: 1,
        text: 'tweet-text',
    }
};

const tweet_response_fulltoken = {
    data: {
        id: 1,
        edit_history_tweet_ids: [],
        author_id: 1,
        text: 'tweet-text',
        pagination_token: 'token'
    }
};


describe('getTweetsByKeyword tests', ()=>{
   
    it('case empty token: test tweets object properties', async () => {

        const startdate = '2022-12-04T00:00:00.000Z';
        const enddate = '2022-12-07T00:00:00.000Z';
        const token = 'empty'; 

        await client.tweets.tweetsFullarchiveSearch.mockReturnValue(tweet_response_emptytoken)

        const expected = await getAttempts(startdate, enddate, token);

        expect(expected).toHaveProperty('data.id');
        expect(expected).toHaveProperty('data.text');
        expect(expected).toHaveProperty('data.author_id');
        expect(expected).toHaveProperty('data.edit_history_tweet_ids'); 
        expect(expected).not.toHaveProperty('data.pagination_token'); 
    })

    it('case full token: test tweets object properties', async () => {

        const startdate = '2022-12-04T00:00:00.000Z';
        const enddate = '2022-12-07T00:00:00.000Z';
        const token = 'token'; 

        await client.tweets.tweetsFullarchiveSearch.mockReturnValue(tweet_response_fulltoken)

        const expected = await getAttempts(startdate, enddate, token);

        expect(expected).toHaveProperty('data.id');
        expect(expected).toHaveProperty('data.text');
        expect(expected).toHaveProperty('data.author_id');
        expect(expected).toHaveProperty('data.edit_history_tweet_ids'); 
        expect(expected).toHaveProperty('data.pagination_token'); 
    })
})
