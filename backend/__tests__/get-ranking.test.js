import getRanking from '../api/get-ranking'
import { client } from "../utils/twitter-client";

jest.mock("../utils/twitter-client");

const tweet_response = {
    data: {
        id: 1,
        edit_history_tweet_ids: [],
        author_id: 1,
        text: 'tweet-text',
    }
};


describe('getRanking tests', ()=>{
    it('valid date', async ()=>{

        const startdate = '2022-11-04T00:00:00.000Z';
        const enddate = '2022-12-05T00:00:00.000Z';
        
        await client.tweets.tweetsFullarchiveSearch.mockReturnValue(tweet_response)

        const expected = await getRanking(startdate, enddate);

        expect(expected).toHaveProperty('data.id');
        expect(expected).toHaveProperty('data.text');
        expect(expected).toHaveProperty('data.author_id');
        expect(expected).toHaveProperty('data.edit_history_tweet_ids');
    })
})
