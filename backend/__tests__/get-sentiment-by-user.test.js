import getSentimentByUser from '../api/get-sentiment-by-user';
import { client } from '../utils/twitter-client';

jest.mock("../utils/twitter-client");

const tweet_response_emptytoken = {
    data: {
        id: 1,
        edit_history_tweet_ids: [],
        author_id: 1,
        text: 'tweet-text',
    }
};

const tweet_response_fulltoken = {
    data: {
        id: 1,
        edit_history_tweet_ids: [],
        author_id: 1,
        text: 'tweet-text',
        next_token: 'token'
    }
};


describe('getSentimentByUser tests', ()=>{
    it('no sense user', async ()=>{

        const noSenseUser = 'from:ioergaherjciksh';
        // casual value
        const startdate = '2022-12-04T00:00:00.000Z';
        const enddate = '2022-12-07T00:00:00.000Z';
        const token = 'empty';

        await client.tweets.tweetsFullarchiveSearch.mockReturnValue({meta: {result_count:0}})

        const expected = await getSentimentByUser(noSenseUser, startdate, enddate, token);

        expect(expected).toStrictEqual({meta: {result_count:0}})
    }) 

    it('case empty token: valid user, test tweets object properties', async () => {

        const validUser = 'from:elonmusk';
        // casual date
        const startdate = '2022-12-04T00:00:00.000Z';
        const enddate = '2022-12-07T00:00:00.000Z';
        const token = 'empty'; 

        await client.tweets.tweetsFullarchiveSearch.mockReturnValue(tweet_response_emptytoken)

        const expected = await getSentimentByUser(validUser, startdate, enddate, token);

        expect(expected).toHaveProperty('data.id');
        expect(expected).toHaveProperty('data.text');
        expect(expected).toHaveProperty('data.author_id');
        expect(expected).toHaveProperty('data.edit_history_tweet_ids'); 
        expect(expected).not.toHaveProperty('data.next_token');
    })

    it('case full token: valid user, test tweets object properties', async () => {

        const validUser = 'from:elonmusk';
        // casual date
        const startdate = '2022-12-04T00:00:00.000Z';
        const enddate = '2022-12-07T00:00:00.000Z';
        const token = 'token'; 

        await client.tweets.tweetsFullarchiveSearch.mockReturnValue(tweet_response_fulltoken)

        const expected = await getSentimentByUser(validUser, startdate, enddate, token);

        expect(expected).toHaveProperty('data.id');
        expect(expected).toHaveProperty('data.text');
        expect(expected).toHaveProperty('data.author_id');
        expect(expected).toHaveProperty('data.edit_history_tweet_ids'); 
        expect(expected).toHaveProperty('data.next_token');
    })
})