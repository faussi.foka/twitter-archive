
import getTeamsFantacitorio from "../api/get-teams-fantacitorio.js";
import { client } from "../utils/twitter-client.js";

jest.mock('../utils/twitter-client.js')

const tweet_response = {
    data: {
        id: 1,
        edit_history_tweet_ids: [],
        author_id: 1,
        text: 'tweet-text',
        attachments: {
            media_keys: [
                1
            ]
        }
    }
};

const user = "from:Fanta_citorio"
const startDate = "2022-10-28T00:00:00.000Z"
const endDate = "2022-10-28T23:59:00.00Z"

describe('getTeamsFantacitorio tests', ()=>{
    it('test object properties', async ()=>{
        const token = "7140w";

        await client.tweets.tweetsFullarchiveSearch.mockReturnValue(tweet_response);

        const expected = await getTeamsFantacitorio(token);

        expect(expected).toHaveProperty('data.id');
        expect(expected).toHaveProperty('data.text');
        expect(expected).toHaveProperty('data.author_id');
        expect(expected).toHaveProperty('data.edit_history_tweet_ids');
        expect(expected).toHaveProperty('data.attachments.media_keys');
    })
})