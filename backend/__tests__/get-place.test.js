import getPlace from "../api/get-place";

describe('getPlace tests', ()=> {
    it('valid id', async ()=> {
        const id = '3797791ff9c0e4c6';

        
        
        const expected = await getPlace(id);

        expect(expected).toHaveProperty('id');
        expect(expected).toHaveProperty('name');
        expect(expected).toHaveProperty('full_name');
        expect(expected).toHaveProperty('country');
        expect(expected).toHaveProperty('country_code');
        expect(expected).toHaveProperty('url');
        expect(expected).toHaveProperty('place_type');
        expect(expected).toHaveProperty('attributes.geotagCount');
        expect(expected).toHaveProperty('bounding_box.type');
        expect(expected).toHaveProperty('bounding_box.coordinates');
        expect(expected).toHaveProperty('centroid');
        expect(expected).toHaveProperty('contained_within.0');
        expect(expected).toHaveProperty('polylines');
        expect(expected).toHaveProperty('geometry');
    })
})