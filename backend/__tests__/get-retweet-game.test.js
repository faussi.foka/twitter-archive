import getRetweetGame from '../api/get-retweet-game'
import { client } from "../utils/twitter-client";

jest.mock("../utils/twitter-client");

const tweet_response = {
    data: {
        id: 1,
        edit_history_tweet_ids: [],
        created_at: '2022-11-08T00:00:00.000Z',
        text: 'tweet-text',
    }
};


describe('getRetweetGame tests', ()=>{
   it('case: invalid id', async ()=>{

        const invalidId = '9';
       
        await client.tweets.findTweetsThatQuoteATweet.mockReturnValue({
            "errors": [
              {
                "value": "invalidId",
                "detail": "Could not find tweet with id: [invalidId].",
                "title": "Not Found Error",
                "resource_type": "tweet",
                "parameter": "id",
                "resource_id": "invalidId",
                "type": "https://api.twitter.com/2/problems/resource-not-found"
              }
            ]
        })

        const expected = await getRetweetGame(invalidId);

        expect(expected).toHaveProperty('errors');
    })

    it('case: valid id, no results', async ()=>{

        const Id = '2097';
       
        await client.tweets.findTweetsThatQuoteATweet.mockReturnValue({meta: {result_count:0}})

        const expected = await getRetweetGame(Id);

        expect(expected).toStrictEqual({meta: {result_count:0}})
    })

    it('case: valid id', async () => {

        const validId = '20';

        await client.tweets.findTweetsThatQuoteATweet.mockReturnValue(tweet_response)

        const expected = await getRetweetGame(validId);

        expect(expected).toHaveProperty('data.id');
        expect(expected).toHaveProperty('data.text');
        expect(expected).toHaveProperty('data.created_at');
        expect(expected).toHaveProperty('data.edit_history_tweet_ids'); 
    })
})
