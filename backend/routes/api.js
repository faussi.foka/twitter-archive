import express from 'express';


const router = express.Router();

import {client, clientv1, userClient, TwitterStreamEvent} from "../utils/twitter-client.js";

import { getEndDate, getToday } from '../utils/get-endDate.js';

import getTweetsByKeyword from '../api/get-tweets-by-keyword.js';
import getTweetsByHashtag from '../api/get-tweets-by-hashtag.js';
import getTweetsByUser from '../api/get-tweets-by-user.js';

import getPlace from '../api/get-place.js';
import getGeoByKeyword from '../api/get-geo-by-keyword.js';
import getGeoByHashtag from '../api/get-geo-by-hashtag.js';
import getGeoByUser from '../api/get-geo-by-user.js';

import getCountDayByKeyword from '../api/get-countday-by-keyword.js';
import getCountDayByHashtag from '../api/get-countday-by-hashtag.js';
import getCountDayByUser from '../api/get-countday-by-user.js';

import getSentimentByKeyword from '../api/get-sentiment-by-keyword.js';
import  getSentimentByHashtag from '../api/get-sentiment-by-hashtag.js';
import  getSentimentByUser from '../api/get-sentiment-by-user.js';

import rankingere from '../utils/rankingghig.js';
import ghigliottina from '../utils/ghigliottina.js';
import getRanking from '../api/get-ranking.js';
import { ranking } from '../utils/fantacitorio.js';
import getTeamsFantacitorio from '../api/get-teams-fantacitorio.js';
import getRetweetGame from '../api/get-retweet-game.js';
import getTermCloud from '../api/get-term-cloud.js';
import createDictionary from '../utils/word-cloud.js';

/**
 * SCACCHI
 */
router.get('/publictweet/:board/:game', async function (req, res) {
  const game = req.params.game
  const start_link = "https://fen2png.com/api/?fen="
  let board = ''
  board = start_link.concat(req.params.board)
  board = convertSpecialCharacters(board)
  const text = 'Game ' + game + '\nRetwitta questo tweet per dirci che mossa faresti te! FORMATO ESEMPIO: "F7 to F6"\n' + convertSpecialCharacters(board) 
  const createdTweet = await userClient.v1.tweet(text+' ', {
    lat: 1.23,
    long: -13.392,
  });
  res.send({id: createdTweet.id_str})
})

function convertSpecialCharacters (s) {
  for (let i = 0; i < s.length; i++) {
    let char = s.charAt(i)
    let sub1 = s.substring(0,i)
    let sub2 = s.substring(i+1)
    if (char == " ") {
      s = sub1 + `+` + sub2
    }
  }
  return s
}

router.get('/getretweetgame/:id', async function (req, res) {
  const id = req.params.id
  const result = await getRetweetGame(id)
  res.send(result)
})

router.get('/maxgamenumber', async function (req, res) {
  const user = 'from:twitt_archive'
  const startdate = getEndDate("2022-12-15")
  const enddate = getEndDate(getToday())
  const result = await getTweetsByUser(user, startdate, enddate, "")
  if (result.meta.result_count == 0) { res.send({max_game_number: 0}); return (0) }
  let max_game = 0
  for (let tweet of result.data) {
    let text = tweet.text.toLowerCase()
    let number = getNumberGame(text)
    if (max_game < number) { max_game = number }
  }
  res.send ({ max_game_number: max_game })
})

function getNumberGame(tweet_text) {
  let phrases = tweet_text.split('\n')
  let words = []
  for (let p of phrases) {
    let tmp = p.split(' ')
    words = words.concat(tmp)
  }
  for (let i = 0; i < words.length - 1; i++) {
    let word = words[i]
    if (word == 'game' && !isNaN(words[i+1])) { 
      return (words[i+1]) 
    }
  } 
  return (0)
}


/**
 * FANTACITORIO
 */
router.get('/getallteams/', async (req,res) => {
  let urls = []
  let next_token = ""
  do {
    const result = await getTeamsFantacitorio(next_token)
    const tweets = result.includes.media
    for (let t of tweets) {
      let img = t.url
      urls.push(img)
    }
    if (Object.hasOwnProperty(result.meta, "next_token")) { next_token = result.meta.next_token }
    else { next_token = "" }
  } while (next_token.length > 0)
  res.send(urls)
})
/*
router.get('/getsingleteam/:id', async (req,res) => {
  let next_token = ""
  let media_key = ""
  let list_media = []
  let user = req.params.id.toLocaleLowerCase()
  let check
  do {
    check = true
    const result = await getTeamsFantacitorio(next_token)
    console.log(result.includes)
    const tweets = result.data
    list_media.push(result.includes.media)
    for (let i = 0; i < tweets.length && check; i++) {
      let t = tweets[i]
      if (user == extractUsername(t.text)) {
        check = false
        if (!t.hasOwnProperty('attachments')) { res.send(); return }
        media_key = t.attachments.media_keys[0]
      }
    }
    if (Object.hasOwnProperty(result.meta, "next_token")) { next_token = result.meta.next_token }
    else { next_token = "" }
  } while (check && next_token.length > 0)
  if (check) { res.send(""); return ; }
  for (let tmp of list_media[0]) {
    if (media_key == tmp.media_key) { res.send(tmp.url); return }
  }
  res.send("")
})
*/ 
router.get('/getsingleteam/:id', async (req,res) => {
  const result = await getTeamsFantacitorio()
  const user =  req.params.id.toLocaleLowerCase()
  let media_key = ""
  for (let tweet of result.data) {
    if (user == extractUsername(tweet.text) && Object.prototype.hasOwnProperty.call(tweet, "attachments")) {
      console.log("If preso")
      media_key = tweet.attachments.media_keys[0]
    }
  }
  for (let t of result.includes.media) {
    if (media_key == t.media_key) {
      res.send(t.url)
      return
    }
  }
})

function extractUsername(text) {
  let tmp = text.substring(0,4)
  if (tmp != "RT @") { return "" }
  text = text.substring(4)
  let cont = 0
  while (text.charAt(cont) != ':' && cont < text.length) { cont++ }
  if (cont < text.length) { return (text.substring(0,cont).toLocaleLowerCase()) }
  return ("")
}


/**
 * TIMELINE
*/

/**
 * KEYWORD 
 * 
 * GET /search 
 * GET /searchnext
 */
router.get('/search/:id/:start/:end/:next_token', async (req,res) => {
    const keyword = req.params.id;
    const startdate = req.params.start + "T00:00:00.000Z";
    const enddate = getEndDate(req.params.end);
    const token = req.params.next_token;

    const result = await getTweetsByKeyword(keyword, startdate, enddate, token); 

    res.send(result);
})

/**
 * HASHTAG
 * 
 * GET /searchbyhashtag
 * GET /searchbyhashtagnext
 */
 router.get('/searchbyhashtag/:id/:start/:end/:next_token', async (req,res) => {
    const hashtag = '#' + req.params.id;
    const startdate = req.params.start + "T00:00:00.000Z";
    const enddate = getEndDate(req.params.end);
    const token = req.params.next_token;

    const result = await getTweetsByHashtag(hashtag, startdate, enddate, token); 

    res.send(result);
})

/**
 * USER
 * 
 * GET /searchbyuser
 * GET /searchbyusernext
 */
 router.get('/searchbyuser/:id/:start/:end/:next_token', async (req,res) => {
    const user = 'from:' + req.params.id;
    const startdate = req.params.start + "T00:00:00.000Z";
    const enddate = getEndDate(req.params.end);
    const token = req.params.next_token;
    const result = await getTweetsByUser(user, startdate, enddate, token);

    res.send(result);
})



/**
 * MAP
 */

/**
 * POSITION
 * 
 * GET /place
 */
 router.get('/place/:id', async (req,res) => {
    const place = req.params.id;

    const result = await getPlace(place); 

    console.log(result)

    res.send(result);
})

/**
 * KEYWORD
 * 
 * GET /searchgeo
 */
 router.get('/searchgeo/:id/:start/:end/', async (req, res) => { 
    const keyword = req.params.id;
    const startdate = req.params.start + "T00:00:00.000Z";
    const enddate = getEndDate(req.params.end);

    const result = await getGeoByKeyword(keyword, startdate, enddate);

    res.send(result);
})

/**
 * HASHTAG
 * 
 * GET /searchgeobyhashtag
 */
 router.get('/searchgeobyhashtag/:id/:start/:end/', async (req, res) => { 
    const hashtag = '#' + req.params.id;
    const startdate = req.params.start + "T00:00:00.000Z";
    const enddate = getEndDate(req.params.end);

    const result = await getGeoByHashtag(hashtag, startdate, enddate);

    res.send(result);
})


/**
 * USER
 * 
 * GET /searchgeobyuser
 */
 router.get('/searchgeobyuser/:id/:start/:end/', async (req, res) => { 
    const user = 'from:' + req.params.id;
    const startdate = req.params.start + "T00:00:00.000Z";
    const enddate = getEndDate(req.params.end);

    const result = await getGeoByUser(user, startdate, enddate);

    res.send(result);
})

/**
 * GRAPHIC
 */

/**
 * KEYWORD
 * 
 * GET /countday
 */
router.get('/countday/:id/:start/:end/:next_token', async (req, res) => {   //GET del numero di tweets per giorno dati: keyword, data inizio, data fine, token nuova pagina IMPORTANTE!!LE DATE VANNO INSERITE CON IL FORMATO AAAA-MM-GGTHH:MM:SS.000Z
    const keyword = req.params.id;
    const startdate = req.params.start + "T00:00:00.000Z";
    const enddate = getEndDate(req.params.end);
    const token = req.params.next_token;

    const result = await getCountDayByKeyword(keyword, startdate, enddate, token);

    res.send(result)
})

/**
 * HASHTAG
 * 
 * GET /countdaybyhashtag
 */
 router.get('/countdaybyhashtag/:id/:start/:end/:next_token', async (req, res) => {   //GET del numero di tweets per giorno dati: keyword, data inizio, data fine, token nuova pagina IMPORTANTE!!LE DATE VANNO INSERITE CON IL FORMATO AAAA-MM-GGTHH:MM:SS.000Z
    const hashtag = '#' + req.params.id;
    const startdate = req.params.start + "T00:00:00.000Z";
    const enddate = getEndDate(req.params.end);
    const token = req.params.next_token;

    const result = await getCountDayByHashtag(hashtag, startdate, enddate, token);

    res.send(result)
  })

  /**
 * USER
 * 
 * GET /countdaybyuser
 */
 router.get('/countdaybyuser/:id/:start/:end/:next_token', async (req, res) => {   //GET del numero di tweets per giorno dati: keyword, data inizio, data fine, token nuova pagina IMPORTANTE!!LE DATE VANNO INSERITE CON IL FORMATO AAAA-MM-GGTHH:MM:SS.000Z
    const user = 'from:' + req.params.id;
    const startdate = req.params.start + "T00:00:00.000Z";
    const enddate = getEndDate(req.params.end);
    const token = req.params.next_token;

    const result = await getCountDayByUser(user, startdate, enddate, token);

    res.send(result)
  })



/**
 * SENTIMENT
 */

/**
 * KEYWORD
 * 
 * GET /sentiment
 */
router.get('/sentiment/:id/:start/:end/:next_token', async (req,res) => {
    const keyword = req.params.id;
    const startdate = req.params.start + "T00:00:00.000Z";
    const enddate = getEndDate(req.params.end);
    const token = req.params.next_token;

    const result = await getSentimentByKeyword(keyword, startdate, enddate, token);

    res.send(result);
})

/**
 * HASHTAG
 * 
 * GET /sentimentbyhashtag
 */
 router.get('/sentimentbyhashtag/:id/:start/:end/:next_token', async (req,res) => {
    const hashtag = '#' + req.params.id;
    const startdate = req.params.start + "T00:00:00.000Z";
    const enddate = getEndDate(req.params.end);
    const token = req.params.next_token;

    const result = await getSentimentByHashtag(hashtag, startdate, enddate, token);

    res.send(result);
})

/**
 * USER
 * 
 * GET /sentimentbyuser
 */
 router.get('/sentimentbyuser/:id/:start/:end/:next_token', async (req,res) => {
    const user = 'from:' + req.params.id;
    const startdate = req.params.start + "T00:00:00.000Z";
    const enddate = getEndDate(req.params.end);
    const token = req.params.next_token;

    const result = await getSentimentByUser(user, startdate, enddate, token);

    res.send(result);
})


/**
 * STREAM
 * GET /stream
 * GET /resultadd
 */
router.get('/stream/:id', async (req, res) => {
    const keyword = req.params.id;

    await client.tweets.addOrDeleteRules({
        "add":[
          {
  
          "tag":keyword,
  
          "value":keyword
        
          }]
        
        });
      
      const stream = await clientv1.v2.searchStream();
  
      // Emitted only on initial connection success
      stream.on(TwitterStreamEvent.Connected, () => console.log('Stream is started.'));
  
      await stream.connect({ autoReconnect: true, autoReconnectRetries: Infinity });

    stream.on(TwitterStreamEvent.Data, async (tweet) => {
        res.write(JSON.stringify(tweet))
        console.log(tweet)
    })
    stream.on(
        // Emitted when a Twitter payload (a tweet or not, given the endpoint).
        TwitterStreamEvent.Data,
        eventData => console.log('Twitter has sent something:', eventData),
    );

    req.on('close', async () => {
        console.log('Stream closed')
        stream.destroy();
        await client.tweets.addOrDeleteRules({
          "delete": {
              "values": [
                keyword
              ]
          }
        });
        res.end()
      });
})

/**
 * RANKINGFantacitorio
 * GET /getranking
 */
 router.get('/getranking/:start/:end', async (req, res) => {
  const startdate = req.params.start + "T00:00:00.000Z";
  const enddate = getEndDate(req.params.end);

  let tweets_text = []
  let next_token = ""
  do {
    let result = await getRanking(startdate, enddate, next_token)
    if (result.meta.result_count == 0) {
      let tmp = {
        "surname": "NESSUN POLITICO",
        "name": "CON PUNTEGGIO",
        "points": "X",
        "position": "X"
      }
      //res.send(ranking([]))
      res.send([tmp])
      return
    }
    for (let data of result.data) {
      let tmp = data.text
      tweets_text.push(tmp)
    }
    if (result.meta.hasOwnProperty('next_token'))  { next_token = result.meta.next_token}
    else { next_token = "" }
  } while (next_token.length > 0)
  let final_result = ranking(tweets_text);
  for (let i = 0; i < final_result.length; i++) {
    final_result[i].position = i + 1
  }
  res.send(final_result)
})

/**
 * WORD CLOUD
 * GET /getwordcloud 
*/
router.get('/getwordcloud/:id/:start/:end', async (req, res) => {
  const keyword = req.params.id;
  const startdate = req.params.start + "T00:00:00.000Z";
  const enddate = getEndDate(req.params.end);
  let result = await getTermCloud(keyword, startdate, enddate, 'empty');
  let tweets_text = [];
  for (let data of result.data) {
    if(data.lang === "it") {
      let tmp = data.text;
      tweets_text.push(tmp);
    }
  }
  
  let final_result = createDictionary(tweets_text);
  res.send(final_result);
})


/**
 * Retrieve word
 * GET /getword
 */
 router.get('/getword/:day', async (req, res) => {
  let startdate= req.params.day + "T17:30:0.000Z"
  let enddate = req.params.day + "T20:00:00.000Z"
  const word = await ghigliottina(startdate,enddate)
  res.send(word)
})



/**
 * Retrieve word
 * GET /getword
 */
router.get('/getfinalrank/:day/:word/:time', async (req, res) => {
  let startdate= req.params.day + "T17:30:0.000Z"
  let wordnow = req.params.word
  let timefinal = req.params.time
  const word = await rankingere(startdate,wordnow, timefinal)
  res.send(word)
})


export default router;















