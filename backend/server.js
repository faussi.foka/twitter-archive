
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import dotenv from 'dotenv';

import router from './routes/api.js';

const app = express();

dotenv.config();

app.use(bodyParser.json());
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use('/api/v1', router);

app.listen(process.env.PORT, console.log(process.env.PORT));