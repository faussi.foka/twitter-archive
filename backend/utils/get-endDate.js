

export function getEndDate(endDate){
    let today = getToday();
    let time = getTime();

    if(endDate === today){ endDate = today + time; }
      else{ endDate = endDate + "T23:59:00.00Z"; }

      return endDate;
}


function getTime(){
    let date = new Date();

    let currentminutes = date.getMinutes()-1;
    if (currentminutes.toString().length == 1) {
      currentminutes = "0" + currentminutes;
    }

    let time = "T" + (date.getHours() - 1) + ":" + currentminutes + ":00.000Z";

    return time;
}

export function getToday(){
    let date = new Date();

    let dd = String(date.getDate()).padStart(2, '0');
    let mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = date.getFullYear();
    let today = yyyy + '-' + mm + '-' + dd;

    return today;
}












