import { Client } from 'twitter-api-sdk';
import { TwitterApi, ETwitterStreamEvent } from 'twitter-api-v2';
import dotenv from 'dotenv';

dotenv.config();

export const client =  new Client(process.env.BEARER_TOKEN); 
export const clientv1 = new TwitterApi(process.env.BEARER_TOKEN);
export const TwitterStreamEvent = ETwitterStreamEvent;

const userClientOld = new TwitterApi({
    appKey: process.env.API_KEY,
    appSecret: process.env.API_SECRET,
    // Following access tokens are not required if you are
    // at part 1 of user-auth process (ask for a request token)
    // or if you want a app-only client (see below)
    accessToken: process.env.ACCESS_TOKEN,
    accessSecret: process.env.ACCESS_SECRET,
});

export const userClient = userClientOld.readWrite
