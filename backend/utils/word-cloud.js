let notAllowed = [
    //articoli
    "il", "lo", "la", "i", "gli", "le", "un", "uno", "una",
    //preposizioni 
    "di", "a", "da", "in", "con", "su", "per", "tra", "fra",
    "del", "dello", "della", "dei", "degli", "delle", "nell",
    "al", "allo", "alla", "ai", "agli", "alle", "all", "ad",
    "dal", "dallo", "dalla", "dai", "dagli", "dalle", "dell",
    "nel", "nello", "nella", "nei", "negli", "nelle",
    "col", "coi",
    "sul", "sullo", "sulla", "sui", "sugli", "sulle",
    //congiunzioni
    "e", "nè", "né", "o", "inoltre", "ma", "però", "dunque", "anzi", "che",
    "allorché", "perché", "giacché", "purché", "affinché", "eppure", "oppure", "dopoché",
    "anche", "neanche", "neppure", "ovvero", "ossia", "ma", "però", "tuttavia", "anzi",
    "infatti", "cioè", "quindi", "perciò", "sia", "come", "quando", "mentre", "finché",
    "poiché", "siccome", "così", "tanto", "più", "sebbene", "se", "qualora", "tranne", 
    "quanti", "quante", "quanto", "meno", "decisamente", "estremamente", "ed", "allora", "finalmente",
    //pronomi
    "io", "tu", "egli", "ella", "essa", "noi", "voi", "loro", "essi", "esse",
    "me", "mi", "te", "ti", "lui", "lei", "ne", "se", "sé", "ci", "ce", "vi", "ve", "li",
    "questo", "questa", "queste", "quello", "quella", "quelle", "quelli", "quegli", "stesso", "stessa", "tali", "ciò", "questi",
    "alcuni", "modo", "alcune", "ciascuno", "ciascuna", "tutto", "tutta", "qualcuno", "qualcuna",
    "poco", "quali", "quale", "dovunque", "chiunque", "chi", "quanta", "ormai", "lì", "sotto", "sopra",
    "mio", "tuo", "suo", "nostro", "vostro", "loro", "mia", "mie", "miei", "tuoi", "tue", "nostri", "nostre", 
    "vostre", "vostri", "vostra", "suoi", "sua", "suo", "nostra", "qualcosa", "quel", "ovunque",
    //intersezioni
    "ah", "oh", "eh", "ehm", "ih", "ahi", "ohi", "ahia", "ahimè", "mah", "boh", "bah",
    "uffa", "toh", "uff", "vabbè", "ooooohhhh",
    //locuzioni avverbiali
    "dirittura", "vanvera", "là", "all'insaputa", "d'altronde", "solito", "emtro", "precedenza", 
    "primis", "prossimità", "seguito", "soldoni", "baleno", "battibaleno", "ogni", "altro", "senz'altro", 
    "l'altro", "tutt'altro", "piuttosto", "pure", "po'", "dopo", "magari", "tanti", "senza",
    //avverbi
    "qui", "qua", "ora", "adesso", "ancora", "ieri", "oggi", "domani", "prima", "poi",
    "presto", "subito", "mai", "tardi", "volentieri", "molto", "niente", "nulla", "là",
    "sì", "certo", "sicuro", "no", "non", "nemmeno", "forse", "probabilmente", "quasi",
    "onestamente", "lentamente", "rapidamente", "bocconi", "carponi", "praticamente", "sicuramente",
    "esattamente", "assurdo", "intanto", "regolarmente", "presumibilmente", "veramente",
    "comunque", "davvero", "proprio", "appena", "sempre", "solo", "insomma", "dietro", "purtroppo",
    "diversamente", "personalmente", "propri", "relativo", "relativamente", "semplicemente", "cromaticamente",
    "improvvisamente",
    //verbi
    "ho", "hai", "è", "ha", "abbiamo", "avete", "hanno", "sono", "sei", "è", "siamo", "siete", "sono",
    "essere", "avere", "so", "renderanno", "fosse", "fa", "va", "chiedo", "chiede",
    "sarò", "sarà", "dato", "sta", "fatto", "può", "stanno", "paghi", "succede", "sostengono",
    "raccontare", "fare", "parla", "arriva", "schiccia", "costituisce", "cerco", "dite",
    "disponevano", "andassero", "sarebbe", "cade", "mette", "discussi", "dovrebbe", "aveva",
    "bussato", "sto", "mandare", "stufi", "cerca", "trova", "entrare", "possano", "uscendo",
    "aspettandosi",  "vuole", "raffigura", "era", "stare", "facciamo", "faccio", "faremo",
    "posso", "poter", "vogliono", "facendo", "sapevo", "fate", "voglio", "stava", "vuol", "abbia", 
    "deve", "trovare", "aspettavo", "capiti", "messo", "avuto", "com'è", "vorrei", "tiene", 
    "devo", "ammettere", "facendola", "darti", "senti", "passati", "detto", "sai", "dice",
    "ero", "abbiano", "bisognerebbe", "do'", "fanno", "prepari", "arriveranno", "fossimo", 
    "avevamo", "stata", "penso", "vedo", "avevo", "stiamo", "dicono", "approvo", "preferisce",
    "saprei", "erano", "avesse", "vieni", "tornano", "usare", "provate", "mettono", "devono", 
    "capita", "ricapitolando", "averlo", "pensate", "utilizzo", "guadagnerà", "finisce", "incontra",
    "esiste", "piaceva", "visto", "fatta", "sembra", "voler", "potrei", "andare", "direi", "pensa",
    "tagliati", "entro", "aver", "guardo", "visita", "frega", "piace", "crepano", "potrebbe", "guarda",
    "vuoi", "ascolta", "do", "notare","diventi", "ricevere", "volevo", "metti", "vado", "stavo",
    "devi", "riesce",  "andato", "potrebbero", "sarei", "prese", "puoi", "fatemela", "diventare",
    "ricordatelo", "percepito", "pensato", "viene", "portare", "colto", "crea", "ruotano", "sappiamo",
    "torna", "bisogna", "spegnere", "rende", "servono", "far", "restiamo", "spetta", "chiedi", "denunciamo",
    "girare", "fonde", "fonda", "funziona", "costituire", "scoperto", "scopra", "aspetto", "prescindere",
    "inciampo", "tentato", "affermare", "parlar", "affermano", "dire", "mandato", "alza", "facciamolo", "uscire",
    "vedere", "preferisco", "giocare", "lasciato", "colpisce", "mandati", "batte", "raggiunge", "esce", "vista",
    "guardalo", "crediamo", "cambieremo", "cambi", "imparare", "trovata", "segna", "asciuga", "scaturisce",
    "riesco", "stai", "manco", "tratta", "farmi", "vedi", "ricordati", "mettere", "tolto", "stato", "risalgono",
    "apriremo", "rifare", "diceva", "sentirla", "notato", "fu", "spero", "manca", "inquadrarlo", "tornato", "parlare",
    "distruggendo", "sparisca", "uscirne", "scegliere", "nato", "spiace", "resto", "conosco", "avrei", "dà",
    "soffro", "gestire", "gaso", "crepare", "dovevo", "torni", "fossi", "tornare", "sceglierà", "riempivate", "son", 
    "imbattermi", "piaci", "arrivate", "ordina", "lottato", "ottenere", "sapete", "potete", "capire", "adoravano",
    "nasce", "soffre", "fucilano", "vede", "recuperare", "riuscita", "ritrova", "vive", "decidono", "mancato", "approvano",
    "risponde", "schiaccia", "dici", "saremo", "credo", "pagata", "proverebbe", "serva", "pubblicate", "troviamo", "dia",
    "pronto", "scappano", "passo", "amo", "salgo", "scendo", "attacccare", "butterei", "arrestati", "venire", "ami",
    "ascoltare", "bevuto", "aspetterò", "cagata", "chiediamo", "tira", "volevano", "anziché", "darli", "include", "vicino",
    "potrai", "tradito", "fai", "frequentata", "vada", "fingere", "continuamo", "occupassi", "corre",
    //vario
    "si", "cose", "cosa", "tutti", "tutta", "tutto", "cui", "ancor", "già", "de", "massimo",
    "cazzo", "vaffanculo", "verso", "preso", "parte", "dove", "deriva", "capito", "vicenda",
    "ferma", "seconda", "durante", "f...", "v...", "d...", "il...", "contro", "via", "&#34;è",
    "casi", "e...", "luca", "cosi", "pero", "nn", "de", "xchè", "secondo", "perchè", "qualche",
    "due", "tre", "altri", "ke", "altre", "ᴄᴇʀᴄᴀ", "ᴏɴʟɪɴᴇ", "ᴛᴇʟᴇɢʀᴀᴍ", "attorno", "esatto", "poche",
    "fino", "tutte", "tipo", "invece", "tanta", "almeno", "🇮🇹", "by", "mei", "soli", "mm", "circa",
    "po", "dentro", "qualsiasi", "cm", "piu", "basta", "attraverso", "priori", "epperò", "volta",
    "troppo", "cmq", "altra", "nonostante", "diversi", "diverse", "palese", "penultima", "from", "to", "with",
    "incorvassi", "meglio", "peggio", "caso", "ca", "and", "cu", "gran", "seguenti", "piene",
    "continuazione", "giù", "pe", "ass", "sola", "sennò", "minchia", "data", "buona", "primo", "fuori",
    //simboli
    "+", "-", "!", "'", "?", "!", "$", "%", "&", "(", ")", "[", "]", "{", "}",
    "/", ":", ".", ",", "_", ";", "<", ">", "=", "&#92;", "rt", "...", "€", "|",
];

let punctuations = ["€", "«", "»", "-", "—", ".", ".", ",", ";", ":", "!", "?", "(", ")", "[", "]", "“", "’", "’", "”", "\"", "*", "/", "\\", "…", "'"];

let articles1 = ["l", "d", "v", "c", "n"];
let articles2 = ["un"];
let articles3 = ["all", "com", "cos"];
let articles4 = ["dell", "dall", "nell", "sull", "mezz"];
let articles5 = ["quell", "quest"];

let dictionary = [];
let item = '';

function initialize (text) {
    text = text.toString().toLowerCase();
    dictionary.length = 0;

    let element = text.split("\n").toString();
    element = element.split(" ").toString();
    element = element.split(",");

    return (element);
}

function wordCleaning () {
    let i = item.length-1;
    let j = 0;

    if (
        (articles5.find(element => element === item.substring(0, 5)) !== undefined) &&
        (punctuations.find(element => element === item[5]) !== undefined)
    ) {
        item = item.slice(6)
    }
    else if(
        (articles4.find(element => element === item.substring(0, 4)) !== undefined) &&
        (punctuations.find(element => element === item[4]) !== undefined)
    ) {
        item = item.slice(5);
    }
    else if(
        (articles3.find(element => element === item.substring(0, 3)) !== undefined) &&
        (punctuations.find(element => element === item[3]) !== undefined)
    ) {
        item = item.slice(4);
    }
    else if(
        (articles2.find(element => element === item.substring(0, 2)) !== undefined) &&
        (punctuations.find(element => element === item[2]) !== undefined)
    ) {
        item = item.slice(3);
    }
    else if(
        (articles1.find(element => element === item[0]) !== undefined) &&
        (punctuations.find(element => element === item[1]) !== undefined)
    ) {
        item = item.slice(2);
    }

    while(punctuations.find(element => element === item[i]) !== undefined) {
        item = item.slice(0, -1);
        i --;
    }
    while(punctuations.find(element => element === item[j]) !== undefined) {
        item = item.slice(1);
        j ++;
    }

}

function sortForDictionary () {
    let check = true;
    while (check) {
        check = false;
        for (let i = 0; i < (dictionary.length - 1); i++) {
            if (dictionary[i].value < dictionary[i+1].value) {
                check = true;
                let tmp = dictionary[i];
                dictionary[i] = dictionary[i+1];
                dictionary[i+1] = tmp;
            }
        }
    }
}

export default function createDictionary (tweets) {   
    let words = initialize (tweets);
    
    for (let word of words) {
        item = word;
        wordCleaning();
        if(
            (notAllowed.find(element => element === item) === undefined) &&
            !(item.length === 1 || item === '') &&
            !(item.includes('@') || item.includes('http')) &&
            (isNaN(item)) 
        ) {
            let check = false;
            for (let element of dictionary) {
                if (element.name === item) {
                    element.value ++;
                    check = true;
                }
            }
            if(!check) {
                dictionary.push({"name": item, "value": 1});
            }
        }
    }
    
    sortForDictionary();
    return(dictionary);
}

