import politics from "./politics.js"

export function ranking (tweets) {
    resetPoints()
    for (let tweet of tweets) {
        addPoints(tweet)
    }
    sortForRanking()
    let result = deletePoliticsWithZeroPoints()
    return(result)
}

//Resetta i punteggi
function resetPoints() {
    for (let politic of politics) {
        politic.points = 0
    }
}

//Dato un tweet, assegna i punti ai politici
function addPoints (tweet) {
    tweet = tweet.toUpperCase()
    let sentences = tweet.split('\n')
    let points = 0
    for (let sentence of sentences) {
        let words = sentence.split(' ')
        let malus = false
        let index = []
        for (let word of words) {
            if (!isNaN(word) && word != '') {
                points = parseInt(word)
            } else if (word === "MALUS") {
                malus = true
            } else {
                iteratePolitics(politics, index, word)
            }
        }
        if (index.length > 1) {
            index = getCorrectPolitic(words, index)
        }
        iteratePoints(index, points, politics, malus)
    }
}

function iteratePolitics(politics, index, word){

    for (let k = 0; k < politics.length; k++) {
        let tmp = politics[k].surname.split(' ')
        let surname
        if (tmp.length == 1 || tmp[0].length > tmp[1].length) { surname = tmp[0] }
        else { surname = tmp[1] }
        if (surname == word) {
            index.push(k)
        }
    }
}

function iteratePoints (index, points, politics, malus){

    for (let i of index) {
        let index_temp = i
        if (points > 0) {
            if (malus) {
                politics[index_temp].points -= points
                if (politics[index_temp].points < 0) { politics[index_temp].points = 0 }
                //console.log("Tolgo " + points + " a " + politics[index_temp].name + " " + politics[index_temp].surname)
            }
            else {
                politics[index_temp].points += points
                //console.log("Aggiungo " + points + " a " + politics[index_temp].name + " " + politics[index_temp].surname)
            }
        }
    }

}




function getCorrectPolitic (words, index_array) {
    for (let word of words) {
        for (let index of index_array) {
            if (word == politics[index].name) { 
                return ([index]) 
            }
        }
    }
    return (index_array)
}

//Ordinamento decrescente in base al punteggio (Bubble Sort)
function sortForRanking () {
    let check = true
    while (check) {
        check = false
        for (let i = 0; i < (politics.length - 1); i++) {
            if (politics[i].points < politics[i+1].points) {
                check = true
                let tmp = politics[i]
                politics[i] = politics[i+1]
                politics[i+1] = tmp
            }
        }
    }
}

//L'array 'politics' deve già essere ordinato!!!
function deletePoliticsWithZeroPoints() {
    let i = 0
    while(politics[i].points > 0) { i++ }
    const result = politics.slice(0, i)
    return result
}
