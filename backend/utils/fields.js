export const userFields = [
    'id',
    'name',
    'username'
]

export const tweetFields = [
    'author_id',
    'id',
    'text',
    'geo',
    'created_at'
]

export const searchCountFields = [
    'end',
    'start',
    'tweet_count'
]

export const numberOfTweets30 = 30;
export const numberOfTweets500 = 500;
export const numberOfTweets100 = 100;