import getWord from "../api/get-word.js";

export default async function ghigliottina (start, end){

    let tweetquizz = await getWord(start, end)

    let todaysword = tweetparser(tweetquizz)

    let todayswordtime = timecheck(tweetquizz)

    let todaysid = tweetquizz.data[0].id

    let response = [todaysid, todaysword, todayswordtime]

    return (response)

}

function tweetparser (result){

    let tweet = result.data[0].text
    let parola 
    let temp
    let sentences = tweet.split('\n')


        for(let sentence of sentences){
            let hash = sentence.search("#parola")
            let eredita = sentence.search("#leredita")
            if(eredita != -1){
                if(hash != -1){
                    temp = sentence.split(': ')
                    temp.shift()
                    parola = temp
                }
            }
        }

    return(parola)
}

function timecheck (tweetquizz){
    
    let date = tweetquizz.data[0].created_at
    return date

}